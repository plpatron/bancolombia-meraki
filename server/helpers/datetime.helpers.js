const datetimeHelpers = {};

function formatDate(date) {
    return `${date.getFullYear()}-${formatMonthDate(date.getMonth() + 1)}-${formatMonthDate(date.getDate())}`;
}

function formatMonthDate(date) {
    return date < 10 ? `0${date}` : date;
}

Object.assign(datetimeHelpers, {formatDate});

module.exports = datetimeHelpers;