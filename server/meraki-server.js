// gcloud debugger
// require('@google-cloud/debug-agent').start();

// server.js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const compression = require('compression');
const path = require('path');
const publicDirectory = process.env.PROD || 'src';
const PORT = process.env.PORT || 8080;

app.use(express.static(`${__dirname}/../dist/bancolombia-meraki/`));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "false");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(compression());

app.use(require('./controllers'));

app.listen(PORT, () => {
    console.log(`Listening port ${PORT}`);
});
