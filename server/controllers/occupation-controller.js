const express = require('express');
const bigquery = require('@google-cloud/bigquery');
const router = express.Router();

const options = {
    query: '',
    useLegacySql: true
}

router.get('/ap/:ap/date/:date', (req, res) => {
    const bigqueryConnection = bigquery({
        projectId: 'brave-builder-795'
    });

    const query = `
    SELECT
        CASE WHEN ap = ''
        THEN 'Ap sin tag asignado'
        ELSE ap
        END AS ap
        HOUR(t) AS hour,
        COUNT(*) AS users
    FROM
        meraki_bancolombia.v_ocupa
    WHERE
        DATE(t) = '${req.params.date}' and ap = '${req.params.ap}'
    GROUP BY
        hour,
        ap
    ORDER BY
        hour ASC`;
    
    Object.assign(options, {query});

    return bigqueryConnection
    .query(options)
    .then(results => {
        res.status(200).json(results[0]);
    })
    .catch( e => {
        res.status(500).send(e);
    })
});

router.get('/date/:date', (req, res) => {
    const bigqueryConnection = bigquery({
        projectId: 'brave-builder-795'
    });

    const query = `
    SELECT
    CASE
        WHEN ap = '' THEN 'Salas'
        WHEN ap = 'Piso10 - Medellin - CDI' THEN 'Entrada'
        WHEN ap = 'Piso10 - CDI - Medellin' THEN 'Laboratorio'
        WHEN ap = 'Medellin - CDI - Piso10' THEN 'Lounge'
        ELSE ap
    END AS ap,
    HOUR(t) AS hour,
    COUNT(*) AS users
    FROM
    meraki_bancolombia.v_ocupa
    WHERE
    DATE(t) = '${req.params.date}'
    GROUP BY
    hour,
    ap
    ORDER BY
    ap DESC,
    hour ASC`;
    
    Object.assign(options, {query});

    return bigqueryConnection
    .query(options)
    .then(results => {
        res.status(200).json(results[0]);
    })
    .catch( e => {
        res.status(500).send(e);
    })
});

module.exports = router;
