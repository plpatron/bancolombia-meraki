const express = require('express')
const router = express.Router();

router.use('/users', require('./users-controller'));
router.use('/occupation', require('./occupation-controller'));
router.use('/prediction', require('./prediction-controller'))

module.exports = router;
