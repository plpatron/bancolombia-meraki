const express = require('express');
const router = express.Router();
const bigquery = require('@google-cloud/bigquery');
const datetimehelpers = require('../helpers/datetime.helpers');

const options = {
    query: '',
    useLegacySql: true
}
router.get('/date/:date', (req, res) => {
    const bigqueryConnection = bigquery({
        projectId: 'brave-builder-795'
    });

    const query = `
    SELECT
        COUNT(DISTINCT sessionId) as users,
        CASE
            WHEN apIn = '' THEN 'Salas'
            WHEN apIn = 'Piso10 - Medellin - CDI' THEN 'Entrada'
            WHEN apIn = 'Piso10 - CDI - Medellin' THEN 'Laboratorio'
            WHEN apIn = 'Medellin - CDI - Piso10' THEN 'Lounge'
        ELSE apIn
        END AS location
    FROM
        meraki_bancolombia.v_session_metrics
    WHERE
        DATE(localEpoch) = '${req.params.date}'
    GROUP BY
        location
    `;

    Object.assign(options, { query });

    return bigqueryConnection
        .query(options)
        .then(results => {
            // TODO: Become this in a function
            res.status(200).json(results[0]);
        })
        .catch(e => {
            res.status(500).json(e);
        });
});

router.get('/connected', (req, res) => {
    // res.status(200).send(validator);
});

module.exports = router;
