const express = require('express');
const router = express.Router();
const bigquery = require('@google-cloud/bigquery');

const options = {
    query: '',
    useLegacySql: true
}


router.get('/start/:start/end/:end', (req, res) => {
    const bigqueryConnection = bigquery({
        projectId: 'brave-builder-795'
    });

    const query = `
    SELECT
        SUM(a) as salas,
        SUM(b) as lounge,
        SUM(c) as laboratorio,
        SUM(d) as entrada,
        t as date
    FROM (
        SELECT
            STRFTIME_UTC_USEC(TIMESTAMP(t),"%Y-%m-%d" ) AS t,
            a,
            b,
            c,
            d
        FROM
            meraki_bancolombia.forecast2
        WHERE
            DATE(t) >= '${req.params.start}'
            AND DATE(t) <= '${req.params.end}'
        GROUP BY
            t,
            a,
            b,
            c,
            d )
    GROUP BY
        date
    ORDER BY date ASC
    `;

    Object.assign(options, { query });

    return bigqueryConnection
        .query(options)
        .then(results => {
            res.status(200).json(results[0]);
        })
        .catch(e => {
            res.status(500).send(e);
        })
});

module.exports = router;