import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './modules/material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AppUsersComponent } from './components/app-users/app-users.component';
import { OccupationComponent } from './components/occupation/occupation.component';
import { PredictionTableComponent } from './components/prediction-table/prediction-table.component';
import { LoadingGifComponent } from './components/loading-gif/loading-gif.component';
import { ChartComponent } from './components/chart/chart.component';
import { DialogComponent } from './components/dialog/dialog.component';

import { AppRoutingModule } from './modules/app-routing/app-routing.module';
import { PlacesComponent } from './components/places/places.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AppUsersComponent,
    OccupationComponent,
    PredictionTableComponent,
    LoadingGifComponent,
    ChartComponent,
    DialogComponent,
    PlacesComponent    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    LayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [ ],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
