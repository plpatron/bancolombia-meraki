import {
    Component,
    OnInit
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { Occupation } from './occupation.interface';
import { DataChart } from '../chart/data-chart.interface';

import { OccupationService } from './occupation.service';

@Component({
    selector: 'bancolombia-occupation',
    styleUrls: ['./occupation.component.scss'],
    templateUrl: './occupation.component.html',
})
export class OccupationComponent implements OnInit {

    aps: string[];
    dataFiltered: Occupation[];
    graphData: DataChart;

    date: FormControl = new FormControl(new Date());
    maxDate: Date = new Date();
    selectedAp: string;

    occupationData: Occupation[];
    chartOptions = {
        responsive: true,
        legend: {
            display: true
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    display: false
                }
            }],
        }
    };
    chartType: string = 'line';

    constructor( private occupationService: OccupationService) { }

    ngOnInit() {
        this.getOccupationData();
    }

    private getOccupationData(dateToSearch?: string) {
        const date = dateToSearch ? dateToSearch : this.formatDate(new Date());
        this.occupationService.getOccupationBydate(date)
            .subscribe( data => {
                this.occupationData = data;
                this.getAps();
                this.filterDataByAp();
                this.createDataset();
            });
    }

    createDataset() {
        this.graphData = {
            labels: this.createLabels(),
            datasets: [
                {
                    label: 'Personas por AP',
                    fill: true,
                    data: this.createGraphData(),
                    backgroundColor: '#bfd7f3',
                    borderColor: '#0956af',
                    lineTension: 0.2,
                    borderWidth: 2
                }
            ]
        };
    }

    filterDataByAp() {
        this.selectedAp = this.selectedAp ? this.selectedAp : this.aps[0];
        this.dataFiltered = this.occupationData.filter( occupation => {
            return occupation.ap === this.selectedAp;
        });
    }

    createLabels(): string[] {
        return this.dataFiltered.map( data => data.hour.toString() );
    }

    createGraphData(): number[] {
        return this.dataFiltered.map( data => data.users );
    }

    getAps() {
        const aps = new Set(this.occupationData.map( occupation => {
            return occupation.ap;
        }));

        this.aps = Array.from(aps);
    }

    apChanged($event) {
        this.filterDataByAp();
        this.createDataset();
    }

    dateChanged() {
        this.getOccupationData(this.formatDate(this.date.value));
    }

    // TODO: find the way to load in the server side and in the front-end side.
    formatDate(date) {
        return `${date.getFullYear()}-${this.formatMonthDate(date.getMonth() + 1)}-${this.formatMonthDate(date.getDate())}`;
    }
    
    formatMonthDate(date) {
        return date < 10 ? `0${date}` : date;
    }

}
