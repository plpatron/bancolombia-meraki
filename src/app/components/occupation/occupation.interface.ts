export interface Occupation {
    ap: string;
    hour: number;
    users: number;
}
