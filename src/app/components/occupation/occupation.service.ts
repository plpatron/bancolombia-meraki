import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Occupation } from './occupation.interface';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { environment } from '../../../environments/environment'

@Injectable({
    providedIn: 'root'
})
export class OccupationService {
    occupationData: any[];

    constructor( private http: HttpClient ) {}

    getOccupationByDateAndAp(date: string, ap: string): Observable<Occupation[]> {
        return this.http.get<Occupation[]>(`${environment.serverAddress}/occupation/ap/${ap}/date/${date}`)
        .pipe(
            retry(2),
            catchError(this.handleError)
        )
    }

    getOccupationBydate(date: string) {
        return this.http.get<Occupation[]>(`${environment.serverAddress}/occupation/date/${date}`)
        .pipe(
            retry(2),
            catchError(this.handleError)
        )
    }

    private handleError(error: HttpErrorResponse ) {
        if (error.error instanceof ErrorEvent) {
            // Connection error occurred, handle this in the right way.
        }else {
            // Backend returned and unsuccessful response code.
            // Get the status code here and also the body.
            console.log(
                `Backend status code: ${error.status}` + 
                `Response body: ${error.error}`
            );
        }
        return throwError(`There was an error, please try again`);
    }

}
