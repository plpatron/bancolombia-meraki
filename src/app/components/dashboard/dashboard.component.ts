import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { DialogComponent } from '../dialog/dialog.component';

@Component({
    selector: 'bancolombia-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    @ViewChild('sidenav') sidenav: MatSidenav;

    today: number = Date.now();
    screenWidth: number;
    colspan: number;

    constructor(
        public breakpointObserver: BreakpointObserver,
        private dialog: MatDialog) {
        this.screenWidth = window.innerWidth;
        this.colspan = 3;
    }

    ngOnInit() {
        this.breakpointObserver
            .observe(['(max-width: 900px)'])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    this.sidenav.close();
                    this.colspan = 4;
                } else {
                    this.sidenav.open();
                    this.colspan = 3;
                }
            });
    }

    openDialog(placeName) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {placeName};
        dialogConfig.panelClass = 'big-panel';
        dialogConfig.disableClose = true;
        this.dialog.open(DialogComponent, dialogConfig);
    }

}
