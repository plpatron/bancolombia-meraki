export interface Predicition {
    salas: number;
    lounge: number;
    laboratorio: number;
    entrada: number;
    date: string;
}
