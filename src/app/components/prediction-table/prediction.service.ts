import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Predicition } from './prediction.interface';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PredictionService {

    constructor(private http: HttpClient) {}

    getPredictions(start: string, end: string): Observable<Predicition[]> {
        return this.http.get<Predicition[]>(`${environment.serverAddress}/prediction/start/${start}/end/${end}`)
        .pipe(
            retry(2),
            catchError(this.handleError)
        )
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // Connection error occurred, handle this in the right way.
        }else {
            // Backend returned and unsuccessful response code.
            // Get the status code here and also the body.
            console.log(
                `Backend status code: ${error.status}` + 
                `Response body: ${error.error}`
            );
        }
        return throwError(`There was an error, please try again`);
    }
}
