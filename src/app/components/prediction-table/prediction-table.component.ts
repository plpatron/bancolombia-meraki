import {
    Component,
    OnInit,
    Inject,
    LOCALE_ID
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { formatDate } from '@angular/common';

import { PredictionService } from './prediction.service';

import { Predicition } from './prediction.interface';
import { DataChart } from '../chart/data-chart.interface';

@Component({
    selector: 'bancolombia-prediction-table',
    templateUrl: './prediction-table.component.html',
    styleUrls: ['./prediction-table.component.scss']
})
export class PredictionTableComponent implements OnInit {

    predictionData: DataChart;
    dateFormatString: string = 'y-MM-dd';

    startDate: FormControl;
    endDate: FormControl;
    minDate: Date = new Date();
    maxDate: Date;

    chartOptions = {
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                }
            }]
        }
    };
    chartType: string = 'bar';

    constructor(private predictionService: PredictionService, @Inject(LOCALE_ID) private locale: string) {
        let nextWeek = new Date();
        nextWeek.setDate(nextWeek.getDate() + 6);
        this.endDate = new FormControl(nextWeek);
        this.startDate = new FormControl(new Date());
        this.maxDate = nextWeek;
    }

    ngOnInit() {
        this.getPredictionData();
    }

    onDateChange() {
        let newEndDate = new Date(this.startDate.value);
        newEndDate.setDate(newEndDate.getDate() + 6);
        this.endDate.setValue(newEndDate);
        this.maxDate = newEndDate;
        this.getPredictionData();
    }

    private getPredictionData() {
        this.predictionService
            .getPredictions(formatDate(this.startDate.value, this.dateFormatString, this.locale), formatDate(this.endDate.value, this.dateFormatString, this.locale))
            .subscribe( prediction => {
                this.predictionData = this.createGraphData(prediction);;
            });
    }

    private createGraphData(data: Predicition[]): DataChart {
        const salas: number[] = data.map(user => {
            return user.salas;
        });

        const lounge = data.map(user => {
            return user.lounge;
        });

        const lab = data.map(user => {
            return user.laboratorio;
        });

        const entrada = data.map(user => {
            return user.entrada;
        });

        const datasetSalas = {
            backgroundColor: '#bfd7f3',
            data: salas,
            label: 'Salas'
        };

        const datasetLounge = {
            backgroundColor: '#80afe6',
            data: lounge,
            label: 'Lounge'
        };

        const datasetLab = {
            backgroundColor: '#4a90e2',
            data: lab,
            label: 'Laboratorio'
        };

        const datasetEntrada = {
            backgroundColor: '#0956af',
            data: entrada,
            label: 'Entrada'
        };

        const labels = data.map(user => {
            return user.date;
        });

        return {
            labels: labels,
            datasets: [datasetSalas, datasetLounge, datasetLab, datasetEntrada]
        };

    }

}
