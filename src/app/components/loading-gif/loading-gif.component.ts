import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

@Component({
    selector: 'bancolombia-loading-gif',
    templateUrl: './loading-gif.component.html',
    styleUrls: ['./loading-gif.component.scss']
})
export class LoadingGifComponent implements OnInit {
    private changes = new BehaviorSubject<Boolean>(false);
    activated: Boolean;

    @Input()
    set activate(value) {
        this.changes.next(value);
    }

    get activate() {
        return this.changes.getValue();
    }

    constructor() {
        this.activated = false;
    }

    ngOnInit() {
        this.changes
        .subscribe( () => {
            this.activated = this.activate;
        });
    }

}
