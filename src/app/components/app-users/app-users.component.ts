import { Component, OnInit } from '@angular/core';
import { User } from './user.interface';
import { AppUsersService } from './app-users.service';

@Component({
    selector: 'bancolombia-app-users',
    templateUrl: './app-users.component.html',
    styleUrls: ['./app-users.component.scss']
})
export class AppUsersComponent implements OnInit {
    
    locations: User[];

    constructor(private appUsersService: AppUsersService) { }

    ngOnInit() {
        let date = new Date();
        this.appUsersService.getUsers(this.formatDate(date)).subscribe( users => {
            this.locations = users;
        });
    }

    connectedUsers(): number {
        return this.locations.reduce( (users, current) => {
            users += current.users; 
            return users;
        }, 0);
    }

    formatDate(date) {
        return `${date.getFullYear()}-${this.formatMonthDate(date.getMonth() + 1)}-${this.formatMonthDate(date.getDate())}`;
    }

    formatMonthDate(date) {
        return date < 10 ? `0${date}` : date;
    }

}
