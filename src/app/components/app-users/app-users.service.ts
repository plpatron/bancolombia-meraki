import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { User } from './user.interface';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppUsersService {

    constructor(private http: HttpClient) {}

    getUsers(date: string):Observable<User[]> {
        // TODO: Put the url in the environments file.
        return this.http.get<User[]>(`${environment.serverAddress}/users/date/${date}`)
        .pipe(
            retry(2),
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse ) {
        if (error.error instanceof ErrorEvent) {
            // Connection error occurred, handle this in the right way.
        }else {
            // Backend returned and unsuccessful response code.
            // Get the status code here and also the body.
            console.log(
                `Backend status code: ${error.status}` + 
                `Response body: ${error.error}`
            );
        }
        return throwError(`There was an error, please try again`);
    }
}