import { User } from './user.interface';

export class UserModel implements User {
    location: string;
    users: number;

    constructor(location: string, users: number) {
        this.location = location;
        this.users = users;
    }

}
