import { Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, ParamMap } from '@angular/router';

@Component({
    selector: 'bancolombia-places',
    templateUrl: './places.component.html',
    styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {
    place: string;

    constructor( private route: ActivatedRouteSnapshot) { }

    ngOnInit() {
        console.log()
        this.route.paramMap.get('place')
    }

}
