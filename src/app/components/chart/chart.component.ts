import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';
import { BehaviorSubject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { DataChart } from './data-chart.interface';

@Component({
    selector: 'bancolombia-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

    @ViewChild('chartElement') chartElement: ElementRef;

    @Input()
    set data(value) {
        this.changes.next(value);
    }

    get data(): DataChart {
        return this.changes.getValue();
    }

    @Input() options: {};
    @Input() type: string;

    private chart: Chart;
    private changes = new BehaviorSubject<DataChart>({labels: [], datasets: []});

    constructor() { }

    ngOnInit() {
        this.createChart();
        this.changes.subscribe( () => {
            if(this.data) {
                this.updateChart();
            }
        });
    }

    createChart(): void {
        this.chart = new Chart(this.chartElement.nativeElement.getContext('2d'), {
            type: this.type,
            data: {
                labels: [],
                datasets: []
            },
            options: this.options
        });
    }

    updateChart(): void {
        this.chart.data = this.data;
        this.chart.update();
    }

}
